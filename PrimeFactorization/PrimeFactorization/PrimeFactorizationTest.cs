﻿using System.Collections.Generic;
using FluentAssertions;
using NUnit.Framework;

namespace PrimeFactorization
{
	[TestFixture]
	public class PrimeFactorizationTest
	{
		private PrimeFactorizationCalculator _calculator;

		[SetUp]
		public void SetUp()
		{
			_calculator = new PrimeFactorizationCalculator();
		}

		public class When_a_number_is_given : PrimeFactorizationTest
		{
            
		    [TestCase(7, new int[] { 7 })]                                       //Factorization of prime numbers
            [TestCase(569, new int[] { 569 })]
            [TestCase(15, new int[] { 3,5 })]                                    //Factorization of numbers without duplicate prime factors
            [TestCase(143, new int[] { 11,13 })]            
            [TestCase(512, new int[] { 2, 2, 2, 2, 2, 2, 2, 2, 2 })]             //Factorization of numbers with duplicate prime factors       
            [TestCase(100, new int[] { 2, 2, 5, 5 })]            
            [TestCase(-12, new int[] {})]                                       //edge cases 
            [TestCase(1, new int[] {})]            
            public void Test_Compute(int number, int[] expectedFactorization)
		    {
		        List<int> result = _calculator.Compute(number);
		        result.Should().BeEquivalentTo(expectedFactorization);
		    }

        }
	}
}
