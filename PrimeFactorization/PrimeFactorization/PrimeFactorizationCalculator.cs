﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using NUnit.Framework.Interfaces;


namespace PrimeFactorization
{
    class PrimeFactorizationCalculator
    {
        public static void Main()
        {
            PrimeFactorizationCalculator primeFactorization = new PrimeFactorizationCalculator();
            primeFactorization.Time();
        }
        public void Time()
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            for (int i = 0; i <= 1000000; i++)
            {
                Compute(i);
                if (i % 1000 == 0)
                {
                    Console.WriteLine(i);
                }
            }
            Console.WriteLine("Total time is " + stopWatch.Elapsed);
            Console.ReadLine();
        }

        public List<int> Compute(int value)
        {
            throw new NotImplementedException();
        }
    }
}
