﻿using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;

namespace FizzBuzz
{
	[TestFixture]
	internal class FizzBuzzGeneratorTests
	{
		private FizzBuzzGenerator _generator;

		[SetUp]
		public void SetUp()
		{
			_generator = new FizzBuzzGenerator();
		}

		public class When_a_numbercollection_is_given : FizzBuzzGeneratorTests
		{
			public class When_a_number_is_divisible_by_3 : When_a_numbercollection_is_given
			{
				[Test]
				public void It_should_print_fizz()
				{
					// Arrange
					int[] input = { 3 };

					// Act
					IEnumerable<string> result = _generator.Generate(input);

					// Assert
					result.Single().Should().Be("Fizz");
				}
			}

			public class When_a_number_is_divisible_by_5 : When_a_numbercollection_is_given
			{
				[Test]
				public void It_should_print_buzz()
				{
					// Arrange
					int[] input = { 5 };

					// Act
					IEnumerable<string> result = _generator.Generate(input);

					// Assert
					result.Single().Should().Be("Buzz");
				}
			}

			public class When_a_number_is_divisible_by_both_3_and_5 : When_a_numbercollection_is_given
			{
				[Test]
				public void It_should_print_fizzbuzz()
				{
					// Arrange
					int[] input = { 15 };

					// Act
					IEnumerable<string> result = _generator.Generate(input);

					// Assert
					result.Single().Should().Be("FizzBuzz");
				}
			}

			public class When_the_collection_contains_a_series_of_numbers : When_a_numbercollection_is_given
			{
				[Test]
				public void It_should_print_fizz_buzz_or_fizzbuzz_where_applicable()
				{
					// Arrange
					IEnumerable<int> input = Enumerable.Range(1, 100);
					IEnumerable<string> expected =
						new[]
						{
							"1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz", "16", "17", "Fizz", "19", "Buzz", "Fizz", "22", "23", "Fizz", "Buzz", "26", "Fizz", "28", "29", "FizzBuzz", "31", "32", "Fizz", "34", "Buzz", "Fizz", "37", "38", "Fizz", "Buzz", "41", "Fizz", "43", "44", "FizzBuzz", "46", "47", "Fizz", "49", "Buzz", "Fizz", "52", "53", "Fizz", "Buzz", "56", "Fizz", "58", "59", "FizzBuzz", "61", "62", "Fizz", "64", "Buzz", "Fizz", "67", "68", "Fizz", "Buzz", "71", "Fizz", "73", "74", "FizzBuzz", "76", "77", "Fizz", "79", "Buzz", "Fizz", "82", "83", "Fizz", "Buzz", "86", "Fizz", "88", "89", "FizzBuzz", "91", "92", "Fizz", "94", "Buzz", "Fizz", "97", "98", "Fizz", "Buzz"
						};

					// Act
					var result = _generator.Generate(input);

					// Assert
					result.Should().BeEquivalentTo(expected);
				}
			}
		}
	}
}