# Inspiration

You could try to implement FizzBuzz while incorporating one (or more) of the following techniques:

* Loops and if statements
* Switch statements
* Recursion
* Lookup tables
* Event delegates & handlers
* Polymorphism
* Whatever design pattern you can find

**Challenge round:** Give your shortest implementation of FizzBuzz

**Here are some FizzBuzz memes for your entertainment**

![](assets/fail.jpg) ![](assets/fizzbuzzcat.jpg)
