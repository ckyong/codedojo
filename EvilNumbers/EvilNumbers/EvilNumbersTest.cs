﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;

namespace EvilNumbers
{
	[TestFixture]
	public class EvilNumbersTest
	{
		private EvilNumbersCalculator _calculator;

		[SetUp]
		public void SetUp()
		{
			_calculator = new EvilNumbersCalculator();
		}

		public class When_a_number_is_given : EvilNumbersTest
		{
			[TestCase(3)]
			[TestCase(43)]
			[TestCase(456)]
			[TestCase(777)]
			[TestCase(873)]
			public void Should_return_true(int valueToCheck)
			{
				bool result = _calculator.Process(valueToCheck);
				result.Should().BeTrue();
			}

			[TestCase(11)]
			[TestCase(55)]
			[TestCase(124)]
			[TestCase(412)]
			[TestCase(666)]
			public void Should_return_false(int valueToCheck)
			{
				bool result = _calculator.Process(valueToCheck);
				result.Should().BeFalse();
			}
		}
		

	}
}
